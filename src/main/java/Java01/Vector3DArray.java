package Java01;

public class Vector3DArray {
    private Vector3D[] vectorS;
    private int size;

    public Vector3DArray(){
        this.vectorS = new Vector3D[0];
        this.size = 0;
    }

    public Vector3DArray(int size){
        this.vectorS = new Vector3D[size];
        for(int i = 0; i < size; i++){
            vectorS[i] = new Vector3D();
        }
        this.size = size;
    }

    public int getSize(){
        return this.size;
    }

    public void changeVector(Vector3D vector, int i){
        this.vectorS[i].Set_x(vector.getX());
        this.vectorS[i].Set_y(vector.getY());
        this.vectorS[i].Set_z(vector.getZ());
    }

    public double lengthOfLongestVector(){
        double len = vectorS[1].getLength();
        for(int i = 1; i < this.size; i++){
            if (vectorS[i].getLength() > len) {
                len = vectorS[i].getLength();
            }
        }
        return len;
    }

    public int findVector(Vector3D needed){
        for(int i = 0; i < this.size; i++){
            if(vectorS[i].equals(needed)){
                return i;
            }
        }
        return -1;
    }



    public Vector3D linearCombination(int[] scalars){
        if(scalars.length == this.size){
            double x = 0, y = 0, z = 0;
            for(int i = 0; i < this.size; i++){
                x += vectorS[i].getX()*scalars[i];
                y += vectorS[i].getY()*scalars[i];
                z += vectorS[i].getZ()*scalars[i];
            }
            Vector3D linearCombination = new Vector3D(x, y, z);
            return linearCombination;
        }
        else{
            Vector3D nullVector = new Vector3D();
            return nullVector;
        }
    }

    public Point3D[] shift(Point3D P){
        Point3D[] pointS = new Point3D[this.size];
        for(int i = 0; i < this.size; i++){
            pointS[i] = new Point3D();
            pointS[i].Set_x(P.getX() + vectorS[i].getX());
            pointS[i].Set_y(P.getY() + vectorS[i].getY());
            pointS[i].Set_z(P.getZ() + vectorS[i].getZ());
        }
        return pointS;
    }
}




























