package Java01;

public class LinearEquations {
    public static void SimiltaneousEquations(double a, double b, double c, double d, double e, double f) {
        if(a*d != c*b){
            double det = a*d - b*c;
            double detx = e*d - f*b;
            double dety = a*f - c*e;
            double x = detx/det, y = dety/det;
            System.out.println("x = " + x);
            System.out.println("y = " + y);
        }
        else if((a == 0 && b == 0 && e == 0 && c == 0 && d == 0 && f != 0) || (a == 0 && b == 0 && e != 0 && c == 0 && d == 0 && f == 0)){
            System.out.println("Система не имеет решений");
        }
        else if((a == 0 && c == 0 && b != 0 && d != 0 && e != 0 && f != 0) && (b*f != e*d)){
            System.out.println("Система не имеет решений");
        }
        else if(a*d == c*b && a*f != c*e){
            System.out.println("Система не имеет решений");
        }
        else if(a*d == c*b && a*f == c*e){
            System.out.println("Система имеет бесконечное множество решений");
        }
    }
}
