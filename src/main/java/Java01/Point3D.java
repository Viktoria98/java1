package Java01;

import java.util.Objects;
import java.util.Scanner;

public class Point3D {
    private double x;
    private double y;
    private double z;

    public Point3D(){
        x = 0;
        y = 0;
        z = 0;
    }

    public Point3D(double _x, double _y, double _z){
        x = _x;
        y = _y;
        z = _z;
    }

    public void Set_x(double _x){
        x = _x;
    }
    public void Set_y(double _y){
        y = _y;
    }
    public void Set_z(double _z){
        z = _z;
    }

    public double getX(){
        return x;
    }
    public double getY(){
        return y;
    }
    public double getZ(){
        return z;
    }

    public void Show_Point(){
        System.out.println(x + " " + y + " " + z);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point3D point3D = (Point3D) o;
        return Double.compare(point3D.x, x) == 0 &&
                Double.compare(point3D.y, y) == 0 &&
                Double.compare(point3D.z, z) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z);
    }
}
