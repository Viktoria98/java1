package Java01;

import java.util.Scanner;

public class Arrays {
    public static void PrintArray(int[] array){
        for(int i = 0; i < array.length; i++){
            System.out.print(array[i] + " ");
        }
        System.out.println();
    }

    public static void FillArray(int n, int[] array){
        Scanner in = new Scanner(System.in);
        for(int i = 0; i < n; i++){
            array[i] = in.nextInt();
        }
    }

    public static int ElementAddition(int n, int[] array){
        int summ = 0;
        for (int value : array) {
            summ += value;
        }
        return summ;
    }

    public static int EvenNumbers(int n, int[] array){
        int even = 0;
        for(int i = 0; i < n; i++){
            if(array[i] % 2 == 0) {
                even++;
            }
        }
        return even;
    }

    public static int Section(int n, int[] array, int a, int b){
        if(a < b){
            int amount = 0;
            for (int value : array) {
                if (value >= a && value <= b) {
                    amount++;
                }
            }
            return amount;
        }
        else {
            return -1;
        }
    }

    public static boolean IsAllPositive(int n, int[] array){
        for(int i = 0; i < n; i++){
            if(array[i] <= 0){
                return false;
            }
        }
        return true;
    }

    public static void Inversion(int[] array){
        for(int i = 0; i < array.length/2; i++){
            int temp = array[i];
            array[i] = array[array.length - i -1];
            array[array.length - i -1] = temp;
        }
    }
}
